#!/usr/bin/python

from math import ceil

def get_reactions(filename):
	lines = open(filename).read().split('\n')[:-1]
	reactions = {}
	for line in lines:
		reactants, product = line.split(' => ')

		reactants = [reactant.split(' ') for reactant in reactants.split(", ")]
		reactants = tuple([(int(quantity), name) for quantity, name in reactants])

		product = product.split(' ')
		product = (int(product[0]), product[1])

		reactions[product[1]] = (reactants, product[0])

	return reactions

def input_needed(reactions, input_name, product_amount, product_name):
	need = dict(zip(reactions, [0] * len(reactions)))
	need[product_name] = product_amount
	need[input_name] = 0
	while any([product_name in reactions and product_name is not input_name
			and need[product_name] > 0 for product_name in need]):
		for product_name in reactions:
			reactants, product_per_batch = reactions[product_name]
			if product_name is not input_name and need[product_name] > 0:
				batches_needed = need[product_name] / product_per_batch
				if need[product_name] % product_per_batch != 0:
					batches_needed += 1

				need[product_name] -= product_per_batch * batches_needed
				for reactant_per_batch, reactant_name in reactants:
					if reactant_name in need:
						need[reactant_name] += reactant_per_batch * batches_needed

	if input_name in reactions:
		need[input_name] -= need[input_name] % -reactions[input_name][1]



	return need[input_name]


reactions = get_reactions("../input.txt")
min_guess = 1
max_guess = 1000000000000
guess = (min_guess + max_guess) / 2
input_needed(reactions, 'ORE', 1, 'FUEL')

curr_result = input_needed(reactions, 'ORE', guess, 'FUEL')
next_result = input_needed(reactions, 'ORE', guess + 1, 'FUEL')

while not (curr_result <= 1000000000000 and next_result > 1000000000000):
	if curr_result <= 1000000000000:
		min_guess = guess
	else:
		max_guess = guess
	guess = (min_guess + max_guess) / 2

	curr_result = input_needed(reactions, 'ORE', guess, 'FUEL')
	next_result = input_needed(reactions, 'ORE', guess + 1, 'FUEL')

print(guess)
