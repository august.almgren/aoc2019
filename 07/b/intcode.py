import csv
import sys
from enum import Enum

class State(Enum):
	READY = 0
	INPUT = 1
	HALTED = 2

class Intcode:
	def load_prgm(filename):
		return [int(i) for i in list(csv.reader(open(filename)))[0]]

	def __init__(self, rom, id = "intcode"):
		self.id = id
		self.ram = rom
		self.ip = 0
		self.inputs = []
		self.outputs = []
		self.state = State.READY

	def add_output(self, fn):
		self.outputs.append(fn)

	def input(self, data):
		self.inputs.append(data)

	def tick(self):
		if self.state != State.HALTED:
			#print_instr(self.ram, self.ip) # trace
			opcode = min(self.ram[self.ip] % 100, len(self.opcodes) - 1)
			self.opcodes[opcode][1](self)

	def getmode(opcode, offset):
		return int(opcode / (10**(offset + 1))) % 10

	# pick right mode to use, return resolved addr
	def getaddr(mem, ip, opcode, offset):
		mode = Intcode.getmode(opcode, offset)
		if mode == 0: # positional
			return mem[ip + offset]
		elif mode == 1: # immediate:
			return ip + offset
		else:
			return None

	def running(self):
		return self.state != State.HALTED

	# opcodes start here

	# no operation: pass
	def op_nop(self):
		if self.state == State.READY:
			self.ip += 1

	# add: p3 = p1 + p2
	def op_add(self):
		if self.state == State.READY:
			opcode = self.ram[self.ip]
			p1 = Intcode.getaddr(self.ram, self.ip, opcode, 1)
			p2 = Intcode.getaddr(self.ram, self.ip, opcode, 2)
			p3 = Intcode.getaddr(self.ram, self.ip, opcode, 3)
			self.ram[p3] = self.ram[p1] + self.ram[p2]
			self.ip += 4

	# multiply: p3 = p1 * p2
	def op_mul(self):
		if self.state == State.READY:
			opcode = self.ram[self.ip]
			p1 = Intcode.getaddr(self.ram, self.ip, opcode, 1)
			p2 = Intcode.getaddr(self.ram, self.ip, opcode, 2)
			p3 = Intcode.getaddr(self.ram, self.ip, opcode, 3)
			self.ram[p3] = self.ram[p1] * self.ram[p2]
			self.ip += 4

	# input: p1 = next(input)
	def op_rcv(self):
		if len(self.inputs) == 0:
			self.state = State.INPUT
		else:
			self.state = State.READY
			opcode = self.ram[self.ip]
			p1 = Intcode.getaddr(self.ram, self.ip, opcode, 1)
			self.ram[p1] = self.inputs.pop(0)
			self.ip += 2

	# output: outputs.append(p1)
	def op_snd(self):
		if self.state == State.READY:
			opcode = self.ram[self.ip]
			p1 = Intcode.getaddr(self.ram, self.ip, opcode, 1)
			for output in self.outputs:
				output(self.ram[p1])
			self.ip += 2

	# branch if equal: if p1 != 0 then ip = p2
	def op_beq(self):
		if self.state == State.READY:
			opcode = self.ram[self.ip]
			p1 = Intcode.getaddr(self.ram, self.ip, opcode, 1)
			p2 = Intcode.getaddr(self.ram, self.ip, opcode, 1)
			if self.ram[p1] != 0:
				p2 = Intcode.getaddr(self.ram, self.ip, opcode, 2)
				self.ip = self.ram[p2]
			else:
				self.ip += 3


	# branch if not equal: if p1 == 0 then ip = p2
	def op_bne(self):
		if self.state == State.READY:
			opcode = self.ram[self.ip]
			p1 = Intcode.getaddr(self.ram, self.ip, opcode, 1)
			p2 = Intcode.getaddr(self.ram, self.ip, opcode, 1)
			if self.ram[self.p1] == 0:
				p2 = getaddr(self.ram, self.ip, opcode, 2)
				self.ip = self.ram[p2]
			else:
				self.ip += 3

	# set if less than: if p1 < p2 then p3 = 1 else p3 = 0
	def op_slt(self):
		if self.state == State.READY:
			opcode = self.ram[self.ip]
			p1 = Intcode.getaddr(self.ram, self.ip, opcode, 1)
			p2 = Intcode.getaddr(self.ram, self.ip, opcode, 2)
			p3 = Intcode.getaddr(self.ram, self.ip, opcode, 3)
			self.ram[p3] = 1 if self.ram[p1] < self.ram[p2] else 0
			self.ip += 4;

	# set if equal: if p1 == p2 then p3 = 1 else p3 = 0
	def op_seq(self):
		if self.state == State.READY:
			opcode = self.ram[self.ip]
			p1 = Intcode.getaddr(self.ram, self.ip, opcode, 1)
			p2 = Intcode.getaddr(self.ram, self.ip, opcode, 2)
			p3 = Intcode.getaddr(self.ram, self.ip, opcode, 3)
			self.ram[p3] = 1 if self.ram[p1] == self.ram[p2] else 0
			self.ip += 4

	# halt: stop program
	def op_hlt(self):
		self.state = State.HALTED

	# the name of each opcode, its callback function, and the instr byte size
	opcodes = (
		("nop", op_nop, 1), # 0
		("add", op_add, 4), # 1
		("mul", op_mul, 4), # 2
		("rcv", op_rcv, 2), # 3
		("snd", op_snd, 2), # 4
		("beq", op_beq, 3), # 5
		("bne", op_bne, 3), # 6
		("slt", op_slt, 4), # 7
		("seq", op_seq, 4), # 8
		("hlt", op_hlt, 1)  # 9+
	)

	def print_instr(mem, ip):
		opcode = min(mem[ip] % 100, len(Intcode.opcodes) - 1)
		next_ip = ip + Intcode.opcodes[opcode][2]
		params = ["[" + str(mem[ip + i])  + "]" if (Intcode.getmode(mem[ip], i) == 0)
				else str(mem[ip + i]) for i in range(1, Intcode.opcodes[opcode][2])]

		sys.stdout.write(".\t" + str(ip) + ":\t")
		sys.stdout.write(Intcode.opcodes[opcode][0] + "\t")
		sys.stdout.write(",\t".join(params))
		print()

	def print_asm(mem):
		ip = 0
		while ip < len(mem):
			Intcode.print_instr(mem, ip)

			opcode = min(mem[ip] % 100, len(Intcode.opcodes) - 1)
			if opcode == len(Intcode.opcodes) - 1:
				break

			ip += Intcode.opcodes[opcode][2]

		print()
