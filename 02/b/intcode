#!/usr/bin/python

from copy import deepcopy
from sys import stdout

# opcode callback functions
# return next ip

def op_nop(ram, ip):
	return ip + 1

def op_add(ram, ip):
	ram[ram[ip + 3]] = ram[ram[ip + 1]] + ram[ram[ip + 2]]
	return ip + 4

def op_mul(ram, ip):
	ram[ram[ip + 3]] = ram[ram[ip + 1]] * ram[ram[ip + 2]]
	return ip + 4

def op_hlt(ram, ip):
	return None

# the name of each opcode, its callback function, and the instr byte size
OPCODES = (
	("NOP", op_nop, 1),
	("ADD", op_add, 4),
	("MUL", op_mul, 4),
	("HLT", op_hlt, 1) # needs to be last
)

def load_prgm(filename):
	input = open(filename)
	# read program from csv file
	prgm = list(input.read()[:-1].split(","))
	# write program into memory and return it
	return [int(i) for i in prgm]

def print_mem(mem):
	print("Mem:")
	print(mem)
	print

def print_asm(mem):
	print("Asm:")

	ip = 0
	while ip < len(mem):
		stdout.write("\t" + str(ip) + ":\t")
		opcode = min(mem[ip], len(OPCODES) - 1)
		next_ip = ip + OPCODES[opcode][2]

		stdout.write(OPCODES[opcode][0] + " ")
		stdout.write(", ".join(map(str, mem[ip + 1 : next_ip])))
		print

		if opcode == len(OPCODES) - 1:
			break

		ip = next_ip

	print

def run_instr(ram, ip):
	opcode = min(ram[ip], len(OPCODES) - 1)
	return OPCODES[opcode][1](ram, ip)

def run_prgm(ram):
	ip = 0
	while ip != None and ip < len(ram):
		ip = run_instr(ram, ip)

def main():
	DESIRED_OUTPUT = 19690720
	ROM = load_prgm("../input.csv")

	for noun in xrange(99):
		for verb in xrange(99):
			ram = deepcopy(ROM)
			ram[1] = noun
			ram[2] = verb

			run_prgm(ram)

			if ram[0] == DESIRED_OUTPUT:
				stdout.write("noun: " + str(noun) + ", ")
				stdout.write("verb: " + str(verb) + ", ")
				stdout.write("result: " + str(100 * noun + verb))
				print
				return

if __name__ == "__main__":
	main()
